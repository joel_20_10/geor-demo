<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'brand', 'model', 'price'];

    public function getPriceAttribute($price)
    {
        return number_format($price, 2, '.', '');
    }
}
