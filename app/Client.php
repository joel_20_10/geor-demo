<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'address', 'city_id', 'last_name', 'dni', 'phone', 'email', 'fullname'];

    public function city()
    {
        return $this->belongsTo('App\City')->select('id', 'name');
    }
}
