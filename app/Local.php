<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Local extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'address', 'city_id'];

    public function city()
    {
        return $this->belongsTo('App\City')->select('id', 'name');
    }
}
