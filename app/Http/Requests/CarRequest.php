<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $name   = ['required', 'max:100', 'string'];
        $method = $this->method();
        if ($method === 'POST') {
            array_push($name, 'unique:cars,name,NULL,id,deleted_at,NULL');
        } else {
            array_push($name, "unique:cars,name,{$this->id},id,deleted_at,NULL");
        }
        return [
            'name'  => $name,
            'brand' => 'required|max:25',
            'model' => 'required|max:25',
            'price' => 'required|numeric',
        ];
    }
}
