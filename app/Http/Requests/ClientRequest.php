<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dni    = ['required', 'digits:8'];
        $method = $this->method();
        if ($method === 'POST') {
            array_push($dni, 'unique:clients,dni,NULL,id,deleted_at,NULL');
        } else {
            array_push($dni, "unique:clients,dni,{$this->id},id,deleted_at,NULL");
        }
        return [
            'name'      => 'required|max:30|string',
            'last_name' => 'required|max:30|string',
            'city_id'   => 'required|numeric|exists:cities,id',
            'dni'       => $dni,
            'address'   => 'max:190',
            'phone'     => 'max:12',
            'email'     => 'max:190',
        ];
    }
}
