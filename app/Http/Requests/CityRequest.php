<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $name   = ['required', 'max:50', 'string'];
        $method = $this->method();
        if ($method === 'POST') {
            array_push($name, 'unique:cities,name,NULL,id,deleted_at,NULL');
        } else {
            array_push($name, "unique:cities,name,{$this->id},id,deleted_at,NULL");
        }
        return [
            'name' => $name,
        ];
    }
}
