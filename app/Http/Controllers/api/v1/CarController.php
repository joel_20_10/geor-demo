<?php

namespace App\Http\Controllers\api\v1;

use App\Car;
use App\Http\Controllers\Controller;
use App\Http\Requests\CarRequest;

class CarController extends Controller
{
    public function index()
    {
        $cars = Car:: select('id', 'name', 'brand', 'model', 'price')
            ->orderBy('name')
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Ok',
            'cars'    => $cars,
        ], 200);
    }

    public function store(CarRequest $carRequest)
    {
        $car = Car::create($carRequest->only('name', 'model', 'brand', 'price'));
        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
            'car'     => $car,
        ], 201);
    }

    public function update(CarRequest $carRequest, $id)
    {
        Car::where('id', $id)
            ->update($carRequest->only('name', 'brand', 'model', 'price'));

        $car = Car::select('id', 'name', 'model', 'brand', 'price')->findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
            'car'     => $car,
        ], 200);
    }

    public function destroy($id)
    {
        Car::where('id', $id)
            ->delete();

        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
        ], 200);
    }
}
