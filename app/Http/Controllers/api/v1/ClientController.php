<?php

namespace App\Http\Controllers\api\v1;

use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;

class ClientController extends Controller
{
    public function index()
    {
        $clients = Client::with('city')
            ->select('id', 'name', 'last_name', 'address', 'city_id', 'phone', 'email')
            ->orderBy('name');
        $filterFullname = request('fullname');
        if ($filterFullname) {
            $clients = $clients->where('fullname', 'like', "%{$filterFullname}%");
        }
        $filterDni = request('dni');
        if ($filterDni) {
            $clients = $clients->where('dni', 'like', "{$filterDni}%");
        }
        $filterPhone = request('phone');
        if ($filterPhone) {
            $clients = $clients->where('phone', 'like', "{$filterPhone}%");
        }
        $filterCity = request('city_id');
        if ($filterCity) {
            $clients = $clients->where('city_id', $filterCity);
        }
        $clients = $clients->get();

        if (request()->wantsJson()) {
            return response()->json([
                'success' => true,
                'message' => 'Ok',
                'clients' => $clients,
            ], 200);
        }
        return view('clients.index', compact('clients'));
    }

    public function store(ClientRequest $clientRequest)
    {
        $clientRequest->merge(['fullname' => "{$clientRequest->name} {$clientRequest->last_name}"]);

        $client = Client::create($clientRequest->only('name', 'last_name', 'fullname', 'phone', 'email', 'dni', 'address', 'city_id'));
        $client->load('city');
        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
            'client'  => $client,
        ], 201);
    }

    public function update(ClientRequest $clientRequest, $id)
    {
        $clientRequest->merge(['fullname' => "{$clientRequest->name} {$clientRequest->last_name}"]);

        Client::where('id', $id)
            ->update($clientRequest->only('name', 'last_name', 'fullname', 'phone', 'email', 'dni', 'address', 'city_id'));

        $client = Client::findOrFail($id);
        $client->load('city');
        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
            'client'  => $client,
        ], 200);
    }

    public function destroy($id)
    {
        Client::where('id', $id)
            ->delete();

        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
        ], 200);
    }
}
