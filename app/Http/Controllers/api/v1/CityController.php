<?php

namespace App\Http\Controllers\api\v1;

use App\City;
use App\Http\Controllers\Controller;
use App\Http\Requests\CityRequest;

class CityController extends Controller
{
    public function index()
    {
        $cities = City::select('id', 'name')
            ->orderBy('name')
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Ok',
            'cities'  => $cities,
        ], 200);
    }

    public function store(CityRequest $cityRequest)
    {
        $city = City::create($cityRequest->only('name'));
        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
            'city'    => $city,
        ], 201);
    }

    public function update(CityRequest $cityRequest, $id)
    {
        $city = City::where('id', $id)
            ->update($cityRequest->only('name'));

        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
            'city'    => $city,
        ], 200);
    }

    public function destroy($id)
    {
        City::where('id', $id)
            ->delete();

        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
        ], 200);
    }
}
