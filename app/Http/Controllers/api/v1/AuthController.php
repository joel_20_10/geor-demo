<?php

namespace App\Http\Controllers\api\v1;

use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $input = $request->only('email', 'password');
        $token = null;

        if (!$token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Nombre de usuario o contraseña incorrectos.',
            ], 401);
        }
        $user = auth()->user();

        return $this->respondWithToken($token, $user, 'Login correcto');
    }

    public function logout()
    {
        request()->validate([
            'token' => 'required',
        ]);

        try {
            JWTAuth::invalidate(request('token'));

            return response()->json([
                'success' => true,
                'message' => 'Usuario desconectado de forma correcta',
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Lo sentimos, la usuario no puede desconectarse',
            ], 500);
        }
    }

    public function refresh()
    {
        $token = JWTAuth::refresh(JWTAuth::getToken());
        return $this->respondWithToken($token);
    }

    protected function respondWithToken($token, User $user = null, string $message = '')
    {
        $responseData = [
            'token' => "Bearer {$token}",
            'expires_in' => Auth::guard()->factory()->getTTL() * 60,
            'success' => true
        ];
        if ($message) {
            $responseData['message'] = $message;
        }
        if ($user) {
            $responseData['user'] = [
                'id'    => $user->id,
                'name'  => $user->name,
                'email' => $user->email,
            ];
        }
        return response()->json($responseData);
    }
}
