<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\LocalRequest;
use App\Local;

class LocalController extends Controller
{
    public function index()
    {
        $locals = Local::with('city')
            ->select('id', 'name', 'address', 'city_id')
            ->orderBy('name')
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Ok',
            'locals'  => $locals,
        ], 200);
    }

    public function store(LocalRequest $localRequest)
    {
        $local = Local::create($localRequest->only('name', 'address', 'city_id'));
        $local->load('city');
        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
            'local'   => $local,
        ], 201);
    }

    public function update(LocalRequest $localRequest, $id)
    {
        Local::where('id', $id)
            ->update($localRequest->only('name', 'address', 'city_id'));

        $local = Local::findOrFail($id);
        $local->load('city');
        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
            'local'   => $local,
        ], 200);
    }

    public function destroy($id)
    {
        Local::where('id', $id)
            ->delete();

        return response()->json([
            'success' => true,
            'message' => 'Información actualizada.',
        ], 200);
    }
}
