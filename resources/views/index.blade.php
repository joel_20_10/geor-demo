<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Reto GEOR</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <link rel="stylesheet" href="{{ mix('css/styles.css') }}">
    <meta name="url" content="{{ url('api/v1') }}">
</head>

<body>
    <div id="app"></div>
    <script src="{{ mix('js/scripts.js') }}"></script>
</body>

</html>
