@extends('layouts.main')

@section('content')
<div class="card">
    <div class="card-header">Lista de clientes</div>
    <div class="card-body">
        <table class="table table-striped table-hover table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Ciudad</th>
                    <th scope="col">Dirección</th>
                    <th scope="col">Telefóno</th>
                    <th scope="col">Correo electrónico</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clients as $key => $client)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $client->name }} {{ $client->last_name }}</td>
                    <td>{{ $client->city->name }}</td>
                    <td>{{ $client->address }}</td>
                    <td>{{ $client->phone }}</td>
                    <td>{{ $client->email }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
