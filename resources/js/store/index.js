import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        submitted: false,
        cities: []
    },
    mutations: {
        SET_SUBMITTED(state) {
            state.submitted = !state.submitted
        },
        SET_CITIES(state, payload) {
            state.cities = payload
        },
        ADD_CITY(state, payload) {
            state.cities.unshift(payload)
        },
        EDIT_CITY(state, payload) {
            state.cities = state.cities.map(c => {
                if (c.id === payload.id) {
                    return payload
                }
                return c
            })
        },
        DELETE_CITY(state, payload) {
            state.cities = state.cities.filter(c => c.id !== payload)
        }
    },
    actions: {
        CHANGE_SUBMITTED(context) {
            context.commit('SET_SUBMITTED')
        },
        SET_CITIES(context, payload) {
            context.commit('SET_CITIES', payload)
        },
        ADD_CITY(context, payload) {
            context.commit('ADD_CITY', payload)
        },
        EDIT_CITY(context, payload) {
            context.commit('EDIT_CITY', payload)
        },
        DELETE_CITY(context, payload) {
            context.commit('DELETE_CITY', payload)
        }
    },
    getters: {
        submitted(state) {
            return state.submitted
        },
        cities: state => state.cities
    }
})
