import Vue from 'vue'
import axios from 'axios'
import store from "../store";

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
axios.defaults.baseURL = document.querySelector("meta[name='url']").getAttribute("content")
axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

const token = localStorage.getItem('token')
if (token) {
    axios.defaults.headers.common['Authorization'] = token
    localStorage.setItem('login', true)
} else {
    localStorage.setItem('login', false)
}

axios.interceptors.request.use((config) => {
    store.dispatch('CHANGE_SUBMITTED')
    return config
}, (error) => {
    return Promise.reject(error);
})

axios.interceptors.response.use((response) => {
    store.dispatch('CHANGE_SUBMITTED')
    return response
}, function (error) {
    const originalRequest = error.config;
    const status = error.response.status
    const data = error.response.data
    if (status === 403) {
        Vue.toasted.info(data.message, {
            icon: {
                name: 'error',
                after: true
            }
        })
    } else if (status === 404) {
        Vue.toasted.info(data.message, {
            icon: {
                name: 'error',
                after: true
            }
        })
    } else if (status === 422) {
        const validationErrors = data.errors
        for (let key in validationErrors) {
            const errorsValidations = validationErrors[key]
            Vue.toasted.info(errorsValidations[0], {
                icon: {
                    name: 'warning',
                    after: true
                }
            })
        }
    } else if (status === 500) {
        Vue.toasted.info(data.message, {
            icon: {
                name: 'error',
                after: true
            }
        })
    } else if (status === 401 && !originalRequest._retry) {
        originalRequest._retry = true;
        return axios.post('/auth/token/refresh')
            .then(response => {
                const data = response.data
                localStorage.setItem('token', data.token);

                axios.defaults.headers.common['Authorization'] = data.token
                originalRequest.headers['Authorization'] = data.token
                return axios(originalRequest);
            })
    }
    store.dispatch('CHANGE_SUBMITTED')
    // return Promise.reject('Paso algo malo en nuestros servidores...')
    return Promise.reject(error.response)
})

Vue.prototype.$axios = axios
