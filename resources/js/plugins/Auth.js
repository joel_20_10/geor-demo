export default function (Vue) {
    Vue.auth = {
        setToken(token, user, expireIn) {
            const date = new Date();
            const dueDateToken = date.setSeconds(
                date.getSeconds() + expireIn
            );
            localStorage.setItem('token', token)
            localStorage.setItem('user', JSON.stringify(user))
            localStorage.setItem("dueDateToken", dueDateToken)
        },
        getToken() {
            const token = localStorage.getItem('token')
            if (!token) {
                return null
            }
            return token
        },
        destroyToken() {
            localStorage.removeItem('token')
            localStorage.removeItem('user')
            localStorage.removeItem("dueDateToken");
        },
        isAuthenticated() {
            if (this.getToken()) {
                return true
            } else {
                return false
            }
        }
    }

    Object.defineProperties(Vue.prototype, {
        $auth: {
            get: () => {
                return Vue.auth
            }
        }
    })
}
