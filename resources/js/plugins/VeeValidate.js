import Vue from 'vue'
import es from 'vee-validate/dist/locale/es.json';
import { ValidationObserver, ValidationProvider, localize, extend } from 'vee-validate'
import { required, email, min } from "vee-validate/dist/rules";

localize('es', es)

extend("required", {
    ...required
});

extend("email", {
    ...email
});

extend('min', {
    validate(value, args) {
        return value.length >= args.length;
    },
    params: ['length']
});

extend('max', {
    validate(value, args) {
        return value.length <= args.length;
    },
    params: ['length']
});

extend("decimal", {
    validate: (value, { decimals = '*', separator = '.' } = {}) => {
        if (value === null || value === undefined || value === '') {
            return {
                valid: false
            };
        }
        if (Number(decimals) === 0) {
            return {
                valid: /^-?\d*$/.test(value),
            };
        }
        const regexPart = decimals === '*' ? '+' : `{1,${decimals}}`;
        const regex = new RegExp(`^[-+]?\\d*(\\${separator}\\d${regexPart})?([eE]{1}[-]?\\d+)?$`);
        return {
            valid: regex.test(value),
            data: {
                serverMessage: 'El campo debe debe ser un número'
            }
        };
    },
    params: ['length'],
    message: `El campo {_field_} debe ser un número y puede contener hasta {length} decimales`
});

Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
