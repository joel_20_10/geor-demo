import Vue from 'vue'
import Router from 'vue-router'
import LoginPage from '../pages/Login'
import HomePage from '../pages/Dashboard'
import CityPage from '../pages/Cities'
import LocalPage from '../pages/Locals'
import CarPage from '../pages/Cars'
import ClientPage from '../pages/Clients'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/login',
            meta: {
                isPublic: true
            }
        },
        {
            path: '/login',
            name: 'Login',
            component: LoginPage,
            meta: {
                isPublic: true
            }
        },
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: HomePage,
            meta: {
                isPublic: false
            },
            children: [
                {
                    path: 'cities',
                    name: 'Cities',
                    component: CityPage
                },
                {
                    path: 'locals',
                    name: 'Locals',
                    component: LocalPage
                },
                {
                    path: 'cars',
                    name: 'Cars',
                    component: CarPage
                },
                {
                    path: 'clients',
                    name: 'Clients',
                    component: ClientPage
                }
            ]
        }
    ]
})
