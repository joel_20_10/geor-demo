import Vue from 'vue'
import vuetify from './plugins/Vuetify'
import router from './router'
import App from './layouts/App'
import Auth from './plugins/Auth'
import Toasted from 'vue-toasted';
import store from './store'

Vue.use(Toasted, {
    theme: "toasted-primary",
    position: "top-right",
    duration: 5000,
    action: {
        text: 'CERRAR',
        onClick: (e, toastObject) => {
            toastObject.goAway(0);
        }
    }
})

require('./plugins/VeeValidate')
require('./plugins/Axios')
Vue.use(Auth)
Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
    const isPublic = to.matched.some(url => {
        return url.meta.isPublic
    })

    const isAuthenticated = Vue.auth.isAuthenticated()
    if (!isPublic && !isAuthenticated) {
        next({ name: 'Login' })
    } else {
        next()
    }
})

new Vue({
    el: '#app',
    router,
    store,
    vuetify,
    render: h => h(App)
})
