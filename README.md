# Proyecto RETO GEOR

## Instrucciones para levantar el demo

1. Clonar el repositorio
2. Instalar la dependencias
    ```sh
    $ composer install
    ```
3. Renombar el .env.example a .env y cambiar las siguientes claves 
    ```sh
    $ DB_DATABASE=custom_db
    $ DB_USERNAME=username_db
    $ DB_PASSWORD=password_db
    ```
4. Ejecutar la migraciones
    ```sh
    $ php artisan migrate
    ```
5. Crear las claves el JWT
    ```sh
    $ php artisan jwt:secret
    ```
6. Ejecutar los seeders
    ```sh
    $ php artisan db:seed
    ```
7. Entrar la url que les dio su servidor
