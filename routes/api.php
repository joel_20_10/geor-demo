<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1'], function () {
    Route::post('auth/login', 'api\v1\AuthController@login');
    Route::post('auth/token/refresh', 'api\v1\AuthController@refresh');

    Route::group(['middleware' => 'auth.jwt'], function () {
        Route::post('auth/logout', 'api\v1\AuthController@logout');

        Route::get('cities', 'api\v1\CityController@index');
        Route::post('cities/store', 'api\v1\CityController@store');
        Route::put('cities/{id}/update', 'api\v1\CityController@update');
        Route::delete('cities/{id}/delete', 'api\v1\CityController@destroy');

        Route::get('locals', 'api\v1\LocalController@index');
        Route::post('locals/store', 'api\v1\LocalController@store');
        Route::put('locals/{id}/update', 'api\v1\LocalController@update');
        Route::delete('locals/{id}/delete', 'api\v1\LocalController@destroy');

        Route::get('cars', 'api\v1\CarController@index');
        Route::post('cars/store', 'api\v1\CarController@store');
        Route::put('cars/{id}/update', 'api\v1\CarController@update');
        Route::delete('cars/{id}/delete', 'api\v1\CarController@destroy');

        Route::get('clients', 'api\v1\ClientController@index');
        Route::post('clients/store', 'api\v1\ClientController@store');
        Route::put('clients/{id}/update', 'api\v1\ClientController@update');
        Route::delete('clients/{id}/delete', 'api\v1\ClientController@destroy');
    });
});
