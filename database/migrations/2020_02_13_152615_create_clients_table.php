<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('city_id');
            $table->string('name', 30);
            $table->string('last_name', 30);
            $table->string('fullname', 70);
            $table->string('dni', 8);
            $table->string('phone', 12)->nullable();
            $table->string('address', 190)->nullable();
            $table->string('email', 190)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('dni');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
