<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now    = now();
        $cities = [
            [
                'name'       => 'Lima',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'name'       => 'Trujillo',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'name'       => 'Chimbote',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'name'       => 'Huaraz',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'name'       => 'Tacna',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ];

        DB::table('cities')->insert($cities);
    }
}
