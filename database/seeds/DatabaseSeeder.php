<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create();
        $this->call(CitiesTableSeeder::class);
        factory(App\Client::class, 100)->create();
    }
}
