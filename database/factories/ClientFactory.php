<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    $name = $faker->firstName;
    $last_name = $faker->lastName;
    return [
        'name'      => $name,
        'last_name' => $last_name,
        'fullname'  => "{$name} {$last_name}",
        'dni'       => $faker->numberBetween(10000000, 99999999),
        'phone'     => $faker->numberBetween(100000000, 999999999999),
        'email'     => $faker->email,
        'address'   => $faker->address,
        'city_id'   => $faker->numberBetween(1, 5),
    ];
});
